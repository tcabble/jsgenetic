<?
// some php logic here

?>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta http-equiv="Cache-control" content="no-cache">
    <title>Test</title>

    <link rel="stylesheet" type="text/css" href="/CSS/myStyle.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

    <script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js'></script>

    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>

    <script language="javascript" type="text/javascript" src="/jSChart/jquery.jqplot.min.js"></script>
    <script type="text/javascript" src="/JsChart/plugins/jqplot.ohlcRenderer.js"></script>
    <script type="text/javascript" src="/jSChart/plugins/jqplot.dateAxisRenderer.js"></script>
    <script type="text/javascript" src="/jSChart/plugins/jqplot.logAxisRenderer.js"></script>
    <script type="text/javascript" src="/jSChart/plugins/jqplot.highlighter.min.js"></script>
    <script type="text/javascript" src="/jSChart/plugins/jqplot.cursor.js"></script>
    <link rel="stylesheet" type="text/css" href="/jSChart/jquery.jqplot.css" />


    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>


    <script type='text/javascript' src='/require/require.js'></script>

    <script type='text/javascript' src='params.js'></script>
    <script type='text/javascript' src='ledger.js'></script>
    <script type='text/javascript' src='individual.js'></script>
    <script type='text/javascript' src='run.js'></script>
    <script type='text/javascript' src='worker.js'></script>
    <script type='text/javascript' src='main.js'></script>




</head>

<script>

var talib = require(['require', './node_modules/talib/build/Release/talib'], function (require) {
        var namedModule = require('./node_modules/talib/build/Release/talib');
        console.log("TALib Version: " + namedModule.version);
    });

// Display all available indicator function names
var functions = talib.functions;
for (i in functions) {
    console.log(functions[i].name);
}

jQuery(document).ready(function() {
    initWorkers();

});

/*
function loadTest() {
    var fileInput = document.getElementById('filePicker');
}
*/

</script>



    <body>

    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">WebSiteName</a>
            </div>
        <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Page 1
                <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#">Page 1-1</a></li>
                    <li><a href="#">Page 1-2</a></li>
                    <li><a href="#">Page 1-3</a></li>
                </ul>
            </li>
            <li><a href="#">Page 2</a></li>
            <li><a href="#">Page 3</a></li>
        </ul>
        </div>
    </nav>


    <div id='wrapper'>

        <div id='formwrapper'>
            <form id='testform'>
                <input type='button' value='click me' onclick='workerTest()'></br>
                <input type='button' value='range is' onclick='getRange()'></br>
                <input type='button' value='test TA' onclick='testTALib()'></br>
                <input id='filePicker' type='file' value='Select File' onchange='loadTest()'>
                <div>
                <p id='displayArea'></p>
                <div id="zoomchartdiv" style="height:400px;width:900px; display:none"></div>
                <div id="mainchartdiv" style="height:150px;width:900px; display:none"></div>

                </div>
                <div >
                <p id='textDisplayArea'></p>
                </div>
            </form>
        </div>
    </div>

    </body>
