self.importScripts('run.js', 'individual.js', 'chartdata.js', 'params.js');

self.chartdata;

self.addEventListener('message', function(e){
    //console.log (e.data);
    if (e.data.cmd == "run") {

        if (self.chartdata == undefined) {
            self.postMessage({'cmd' : 'error', 'message' : 'No data Loaded...'});
            return;
        }

        //var params = e.data.msg;
        var pop = [];
        var run = new Run(self.chartdata, 0);
        run.createRandomPopulation();
        for (var i = 0; i < run.population.length; i++) {
            pop.push(run.population[i].printIndividual());
        }
        //var pop = run.population;
        //var pop = JSON.stringify(run.population);
        self.postMessage({'cmd' : 'print', 'population' : pop});
    } else if (e.data.cmd == "loadData") {

        var file = e.data.fileObj;
        var textType = /text.*/;
        if (file.type.match(textType)) {
            var chartData = [];
            var reader = new FileReader();
            reader.onload = function(e) {
                var linesArray = reader.result.split("\n");
                linesArray.forEach(function (item, index){
                    if (index != 0) {
                        var tempHolder = item.match(/\S+/g);
                        var bar = new Bar();
                        bar.OPEN = tempHolder[0];
                        bar.HIGH = tempHolder[1];
                        bar.LOW = tempHolder[2];
                        bar.CLOSE = tempHolder[3];
                        bar.VOLUME = tempHolder[4];
                        bar.STARTTIME = tempHolder[5];
                        /*
                        var bar = {
                            open    : tempHolder[0],
                            high    : tempHolder[1],
                            low     : tempHolder[2],
                            close   : tempHolder[3],
                            vol     : tempHolder[4],
                            date    : tempHolder[5]
                        }
                        */
                        chartData.push(bar);
                    }
                });
                self.chartdata = new ChartData(chartData);
                self.postMessage({'cmd' : 'chart', 'data' : chartData});
            }
            reader.readAsText(file);


        } else {
            console.log("something wrong");
            //fileDisplayArea.innerText = "File not supported!"
        }
//  }

    } else {
        console.log(e.data.cmd);
        self.postMessage('No recoginized command...');
    }
    //self.close();

}, false);

