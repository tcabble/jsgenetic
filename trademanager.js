function TradeManager (evaluator, indiv, startIndex, stopIndex) {

    this.currentIndex;
    this.stopIndex;
	this.evaluator;
    this.indiv;
	this.dir;
	this.currentTradeDrawdown;
	this.currentTradeReturn;
	this.maxDrawdown;
	this.totalReturn;
	this.currentTrade;

	this.getEvaluator = function() {
	    return this.evaluator;
	};

    this.getIndividual = function() {
        return this.indiv;
	};

    this.runTrades = function(mode) {
	};

    this.getCurrentTrade = function() {
        return this.currentTrade;
	};

	this.manageTrade = function(mode) {
        this.newTrade();
        this.eval.reset(this.currentIndex, this.getIndividual(), this.currentTrade, mode);
        this.dir = this.evaluator.run();
        if (dir == 1) {
            this.currentTrade.setDirection("Long");
            Bar openBar = this.getEvaluator().getBarAt(this.currentIndex - 1);
            this.currentTrade.setOpenValue(openBar.OPEN);
        } else {
            this.currentTrade.setDirection("Short");
        }
        while (this.currentTrade.isOpen() && this.currentIndex > this.stopIndex) {
            this.step();
        }
        if (this.currentIndex > this.stopIndex) {
            this.indiv.getLedgers(mode).addTrade(this.currentTrade);
        }
	};

    this.closeTrade = function(curBar, mode) {
        this.currentTrade.setOpen(false);
        this.currentTrade.setCloseDate(curBar.STARTTIME);
        switch (mode) {
            case 0: //No Stop or Target

                //std::cout << "Open Value: " << this.currentTrade.getOpenValue() << " Close Value: " << this.currentTrade.getCloseValue() << std::endl;
                if (this.dir == 1) {
                    this.currentTrade.setCloseValue(curBar.OPEN);
                    this.currentTrade.setPositionReturn(this.currentTrade.getCloseValue() - this.currentTrade.getOpenValue());
                } else {
                    this.currentTrade.setCloseValue(curBar.OPEN + this.currentTrade.getSpread());
                    this.currentTrade.setPositionReturn(this.currentTrade.getOpenValue() - this.currentTrade.getCloseValue());
                }

                break;
            case 1: //stops out
                if (this.dir == 1) {
                    this.currentTrade.setCloseValue(this.currentTrade.getLongSL());
                    //std::cout << "Open Value: " << this.currentTrade.getOpenValue() << " Close Value: " << this.currentTrade.getCloseValue() << std::endl;
                    this.currentTrade.setPositionReturn(this.currentTrade.getCloseValue() - this.currentTrade.getOpenValue());
                } else {
                    this.currentTrade.setCloseValue(this.currentTrade.getShortSL());
                    //std::cout << "Open Value: " << this.currentTrade.getOpenValue() << " Close Value: " << this.currentTrade.getCloseValue() << std::endl;
                    this.currentTrade.setPositionReturn(this.currentTrade.getOpenValue() - this.currentTrade.getCloseValue());
                }
                break;
            case 2: //take profit
                if (this.dir == 1) {
                    this.currentTrade.setCloseValue(this.currentTrade.getLongTP());
                    //std::cout << "Open Value: " << this.currentTrade.getOpenValue() << " Close Value: " << this.currentTrade.getCloseValue() << std::endl;
                    this.currentTrade.setPositionReturn(this.currentTrade.getCloseValue() - this.currentTrade.getOpenValue());
                } else {
                    this.currentTrade.setCloseValue(this.currentTrade.getShortTP());
                    //std::cout << "Open Value: " << this.currentTrade.getOpenValue() << " Close Value: " << this.currentTrade.getCloseValue() << std::endl;
                    this.currentTrade.setPositionReturn(this.currentTrade.getOpenValue() - this.currentTrade.getCloseValue());
                }
                break;
        }
	};

	this.step = function() {
	    this.currentIndex--;
        if (this.currentIndex <= this.stopIndex) {
            return;
        }
        this.evaluator.step();
        Bar currentBar = this.evaluator.getCurrentBar();

        if(this.dir == 1) {
            if (currentBar.LOW <= this.currentTrade.getLongSL() && this.currentTrade.getLongSL() != 0) {
                this.closeTrade(currentBar, 1);
                return;
            }
            if (currentBar.HIGH >= this.currentTrade.getLongTP() && this.currentTrade.getLongTP() != 0) {
                this.closeTrade(currentBar, 2);
                return;
            }
        } else if(this.dir == -1) {
            if (currentBar.HIGH + this.currentTrade.getSpread() >= this.currentTrade.getShortSL() && this.currentTrade.getShortSL() != 0) {
                this.closeTrade(currentBar, 1);
                return;
            }
            if (currentBar.LOW + this.currentTrade.getSpread() <= this.currentTrade.getShortTP() && this.currentTrade.getShortTP() != 0) {
                this.closeTrade(currentBar, 2);
                return;
            }
        }
        if(this.dir == 1) {
            if (this.currentTrade.getPositionDrawdown() < (this.currentTrade.getOpenValue() - currentBar.LOW)) {
                this.currentTrade.setPositionDrawdown(this.currentTrade.getOpenValue() - currentBar.LOW);
            }
        } else if(this.dir == -1) {
             if (this.currentTrade.getPositionDrawdown() < (currentBar.HIGH - this.currentTrade.getOpenValue())) {
                this.currentTrade.setPositionDrawdown(currentBar.HIGH - this.currentTrade.getOpenValue());
            }
        }
        float currentDir = this.evaluator.run();
        //std::cout << std::endl;
        if (currentDir != this.dir) {
            Bar closeBar = this.getEvaluator().getBarAt(this.currentIndex - 1);
            this.closeTrade(closeBar, 0);
        }
	};

    this.newTrade = function() {
        Bar openBar = this.evaluator.getBarAt(this.currentIndex - 1);
        Trade newTrade(openBar.STARTTIME, "0:00", openBar.OPEN, this.currentIndex - 1);
        this.setTrade(newTrade);
	};

    this.setTrade = function(newTrade) {
        this.currentTrade = newTrade;
	};
}



function FitnessFunction () {

    this.calcFitness = function (evaluator, indiv, range) {
        indiv.getLedgers(0).clearTrades();
        var thisManager = new TradeManager(evaluator, indiv , range[0], range[1]);
        thisManager.runTrades(0);
        var totalReturn = indiv.getLedgers(0).getTotalReturn();
        var maxDrawdown = indiv.getLedgers(0).getMaxDrawdown();

        if (indiv.getLedgers(0).tradeRegister.length < 25 || indiv.getLedgers(0).getTradeDistribution() < 0.15 || maxDrawdown == 0) {
            indiv.fitness = -11111111111111.0;
            indiv.equity = totalReturn;
        } else {
            if (maxDrawdown > 0) {
                indiv.equity = totalReturn;
                indiv.fitness = totalReturn / maxDrawdown - 1.54 * indiv.program.length;
            } else {
            }

        }
    };

    this.calcValFitness = function (evaluator, indiv, range) {
        indiv.getLedgers(1).clearTrades();
        var thisManager = new TradeManager(evaluator, indiv , range[0], range[1]);
        thisManager.runTrades(1);
        var totalReturn = indiv.getLedgers(1).getTotalReturn();
        var maxDrawdown = indiv.getLedgers(1).getMaxDrawdown();

        if (indiv.getLedgers(1).tradeRegister.length < 25 || indiv.getLedgers(0).getTradeDistribution() < 0.15 || maxDrawdown == 0) {
            indiv.valFitness = -11111111111111.0;
            indiv.valEquity = totalReturn;
        } else {
            if (maxDrawdown > 0) {
                indiv.valEquity = totalReturn;
                indiv.valFitness = totalReturn / maxDrawdown - 1.54 * indiv.program.length;
            } else {
            }

        }
    };

    this.calcTestFitness = function(evaluator, indiv, range) {
        std::pair<int, int> range = eval.dataStore.getTestingRange();
        indiv.getLedgers(2).clearTrades();
        var thisManager = new TradeManager(evaluator, indiv , range[0], range[1]);
        thisManager.runTrades(2);
        var totalReturn = indiv.getLedgers(2).getTotalReturn();
        var maxDrawdown = indiv.getLedgers(2).getMaxDrawdown();
        indiv.testReturn = totalReturn;
        indiv.testFitness = totalReturn / maxDrawdown  - 1.54 * indiv.program.size();
    };
}



function Evaluator (historyData, currentIndex) {

    this.dataStore;
    this.PC;
    this.currentDataIndex;
    this.indiv;
    this.workingTrade;
    this.indicatorIndex = [];

    this.run = function(){
        std::string program = this.indiv.program;

        char node = program[this.PC];


        this.PC++;

        int decision = 0;
        int then = 0;
        int elseThen = 0;
        int arg1 = 0, arg2 = 0, arg3 = 0, arg4 = 0, arg5 = 0, indicatorIndex = 0;
        //std::function<float(int&, int&, int&, int&, int&)> indicator;
        int a, b;
        double intpart;
        float fracpart;
        float tempResult = 0.0;

        if (node < 90 ){
            return ((int)node);
        }
        switch(node){
             case 99:
                //std::cout<<"HEAD ";
                decision = run();
                //std::cout<<"Should go LONG ";
                run();
                //std::cout<<"Should go SHORT ";
                run();
                return decision;
            case 100:
                //std::cout<<"IFELSE ";
                if (run() == 1) {
                    then = run();
                    run();
                    return then;
                } else {
                    run();
                    elseThen = run();
                    return elseThen;
                }
            case 101:
                //std::cout<<"AND ";
                a = run();
                b = run();
                if (a == 1 && b == 1) {
                    return 1;
                } else {
                    return -1;
                }
            case 102:
                //std::cout<<"OR ";
                a = run();
                b = run();
                if (a == 1 || b == 1) {
                    return 1;
                } else {
                    return -1;
                }
            case 103:
                //std::cout<<"XOR ";
                a = run();
                b = run();
                if ((a == 1 && b == -1) || (a == -1 && b == 1)) {
                    return 1;
                } else {
                    return -1;
                }
            case 104:
                //std::cout<<"LESS ";
                a = run();
                b = run();
                if (a < b) {
                    return 1;
                } else {
                    return -1;
                }
            case 105:
                //std::cout<<"GREAT ";
                a = run();
                b = run();
                 if (a > b) {
                    return 1;
                } else {
                    return -1;
                }
            case 106:
                //std::cout<<"EQU ";
                a = run();
                b = run();
                 if (a == b) {
                    return 1;
                } else {
                    return -1;
                }
            case 107:
                //std::cout<<"ADD ";
                a = run();
                b = run();
                return(a + b);
            case 108:
                //std::cout<<"SUB ";
                a = run();
                b = run();
                return(a - b);
            case 109:
                //std::cout<<"MUL ";
                return(run() * run());
            case 110:
                //std::cout<<"DIV ";
                {
                float num = run();
                float den = run();
                if (std::fabs(den)< 0.0000001) return (-1.0); //protected division
                else return (num/den);
                }
            case 111:
                //std::cout<<"MIN ";
                return(std::min(run(), run()));
            case 112:
                //std::cout<<"MAX ";
                return(std::max(run(), run()));
            case 113:
                //std::cout<<"NEG ";
                return -(run());
            case 114:
                //std::cout<<"SIN ";
                return (sin(run()));
            case 115:
                //std::cout<<"COS ";
                return (cos(run()));
            case 116:
                //std::cout<<"TAN ";
                return (tan(run()));
            case 117:
                //std::cout<<"LOG ";
                return (log(std::fabs(run())));
            case 118:
                //std::cout<<"MA ";
                indicatorIndex = this.getIndicatorIndex(0);
                //std::cout<<" " << indicatorIndex << " ";
                arg1 = run();
                arg2 = run();
                arg3 = run();
                arg4 = run() + this.currentDataIndex;
                arg5 = run();
                indicator = this.getIndicator(indicatorIndex);
                tempResult = indicator(arg1, arg2, arg3, arg4, arg5);
                //std::cout << " " << tempResult << " ";
                return tempResult;
                //return indicator(arg1, arg2, arg3, arg4, arg5);
                /*std::cout<<"MA ";
                arg1 = run();
                arg2 = run();
                arg3 = run();
                return this.ma(arg1, arg2, arg3);
                */
            case 119:
                //std::cout<<"MACD ";
                indicatorIndex = this.getIndicatorIndex(1);
                //std::cout<<" " << indicatorIndex << " ";
                arg1 = run();
                arg2 = run();
                arg3 = run();
                arg4 = run() + this.currentDataIndex;
                arg5 = run();
                indicator = this.getIndicator(indicatorIndex);
                tempResult = indicator(arg1, arg2, arg3, arg4, arg5);
                //std::cout << " " << tempResult << " ";
                return tempResult;
                //return indicator(arg1, arg2, arg3, arg4, arg5);
                /*std::cout<<"MACD ";
                arg1 = run();
                arg2 = run();
                arg3 = run();
                arg4 = run();
                arg5 = run();
                return this.macd(arg1, arg2, arg3, arg4, arg5);
                 */
            case 120:
                //std::cout<<"STOCH ";
                indicatorIndex = this.getIndicatorIndex(2);
                //std::cout<<" " << indicatorIndex << " ";
                arg1 = run();
                arg2 = run();
                arg3 = run();
                arg4 = run() + this.currentDataIndex;
                arg5 = run();
                indicator = this.getIndicator(indicatorIndex);
                tempResult = indicator(arg1, arg2, arg3, arg4, arg5);
                //std::cout << " " << tempResult << " ";
                return tempResult;
                //return indicator(arg1, arg2, arg3, arg4, arg5);
                /*std::cout<<"STOCH ";
                arg1 = run();
                arg2 = run();
                arg3 = run();
                arg4 = run();
                arg5 = run();
                return this.stoch(arg1, arg2, arg3, arg4, arg5);
                */
            case 121:
                //std::cout<<"NEXT ";
                indicatorIndex = this.getIndicatorIndex(3);
                //std::cout<<" " << indicatorIndex << " ";
                arg1 = run();
                arg2 = run();
                arg3 = run();
                arg4 = run() + this.currentDataIndex;
                arg5 = run();
                indicator = this.getIndicator(indicatorIndex);
                tempResult = indicator(arg1, arg2, arg3, arg4, arg5);
                //std::cout << " " << tempResult << " ";
                return tempResult;
                //return indicator(arg1, arg2, arg3, arg4, arg5);
                /*std::cout<<"RSI ";
                arg1 = run();
                arg2 = run();
                return this.rsi(arg1, arg2);
                */
            case 122:
                //std::cout<<"LAST ";
                indicatorIndex = this.getIndicatorIndex(4);
                //std::cout<<" " << indicatorIndex << " ";
                arg1 = run();
                arg2 = run();
                arg3 = run();
                arg4 = run() + this.currentDataIndex;
                arg5 = run();
                indicator = this.getIndicator(indicatorIndex);
                tempResult = indicator(arg1, arg2, arg3, arg4, arg5);
                //std::cout << " " << tempResult << " ";
                return tempResult;
                //return indicator(arg1, arg2, arg3, arg4, arg5);
                /*std::cout<<"MOM ";
                arg1 = run();
                arg2 = run();
                return this.mom(arg1, arg2);
                */
            case 123:
                //std::cout<<"OPEN ";
                return this.open(run());
            case 124:
                //std::cout<<"HIGH ";
                return this.high(run());
            case 125:
                //std::cout<<"LOW ";
                return this.low(run());
            case 126:
                //std::cout<<"CLOSE ";
                return this.close(run());
            case 127:
                //std::cout<<"VOLUME ";
                return this.volume(run());
            case 90:
                //std::cout<<"--LONG-- ";
                fracpart = run();
                //std::cout << "SL: " << fracpart << " ";
                this.workingTrade.setLongSL(fracpart, this.getBarAt(this.currentDataIndex - 1));//this.getBarAt(this.currentDataIndex - 1)
                fracpart = run();
                //std::cout << "TP: " << fracpart << " ";
                this.workingTrade.setLongTP(fracpart, this.getBarAt(this.currentDataIndex - 1));//this.getCurrentBar()
                return 1;
            case 91:
                //std::cout<<"--SHORT-- ";
                fracpart = run();
                //std::cout << "SL: " << fracpart << " ";
                this.workingTrade.setShortSL(fracpart, this.getBarAt(this.currentDataIndex - 1));//this.getCurrentBar()
                fracpart = run();
                //std::cout << "TP: " << fracpart << " ";
                this.workingTrade.setShortTP(fracpart, this.getBarAt(this.currentDataIndex - 1));//this.getCurrentBar()
                return 1;
            case 92:
                //std::cout<<"SL ";
                fracpart = modf(run(), &intpart);
                fracpart = std::fabs(fracpart) * this.dataStore.getAverageRange() * 2000;
                return fracpart;
            case 93:
                //std::cout<<"TP ";
                fracpart = modf(run(), &intpart);
                fracpart = std::fabs(fracpart) * this.dataStore.getAverageRange() * 2000;
                return fracpart;
        }
        return (0);
    };

    this.test = function(){
    };

    this.reset = function(index, indiv, newTrade, mode) {
        this.PC = 0;
        this.currentDataIndex = index;
        this.indiv = indiv;
        this.workingTrade = newTrade;
        this.setDealSize(mode);
    };

    this.setCurrentIndex = function(index) {
        this.currentDataIndex = index;
    };

    this.step = function() {
        this.currentDataIndex--;
        this.PC = 0;
    };

    this.getCurrentBar = function() {
        return this.dataStore.getBar(this.currentDataIndex);
    };

    this.getBarAt = function(index) {
        return this.dataStore.getBar(index);
    };

    this.setDealSize = function(mode) {
    };

    this.addIndicator = function(masterIndex) {
        this.indicatorIndex.push_back(masterIndex);
    };

    //Indi* getIndividual();

    //void setIndividual(Indi *indiv);
    //std::function<float(int&, int&, int&, int&, int&)> getIndicator(int index);

    this.getIndicatorIndex = function(int index) {
        return this->indicatorIndex.at(index);
    };

    this.open = function(index){
        return this.dataStore.getBar(this.currentDataIndex + index).OPEN;
    };

    this.high = function(index){
        return this.dataStore.getBar(this.currentDataIndex + index).HIGH;
    };

    this.low = function(index) {
        return this.dataStore.getBar(this.currentDataIndex + index).LOW;
    };

    this.close = function(index) {
        return this.dataStore.getBar(this.currentDataIndex + index).CLOSE;
    };

    this.volume = function(index) {
        return this.dataStore.getBar(this.currentDataIndex + index).VOLUME;
    };
}






