function ChartData(data) {
    this.chartdata = data;

    this.getBar = function (index) {
        return this.chartdata[index];
    }
}

function Bar() {
    this.OPEN;
    this.HIGH;
    this.LOW;
    this.CLOSE;
    this.VOLUME;
    this.STARTTIME;
}
