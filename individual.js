function Individual() {

    this.program = "";
    this.ledgers = [3];
    this.fitness = 0;
    this.equity = 0;
    this.valFitness = 0;
	this.valEquity = 0;
    this.testFitness = 0;
    this.testReturn = 0;

    this.printIndividual = function() {
        //console.log("New Individual");
        var indiv = {buffer : ""};
        this.recPrintIndividual(this.program, indiv, 0);
        return indiv.buffer;
    };

    this.setLedger = function(ledger, index) {
        this.ledgers[index] = ledger;
    }

    this.getLedger = function(index) {
        return this.ledgers[index];
    }

    this.recPrintIndividual = function(tempProgram, indiv, bufferCnt) {
        var a1 = 0,a2 = 0, a3 = 0, a4 = 0, a5 = 0;

        //console.log (tempProgram.charCodeAt(bufferCnt), " : ", bufferCnt);
        if (tempProgram.charCodeAt(bufferCnt) < 90){//this->test->FSET_START){

            indiv.buffer += "val=" + tempProgram.charCodeAt(bufferCnt++) + ",";

            //console.log("Returning: ", bufferCnt);

            return(bufferCnt);
        }
        //std::cout << "In Here" << std::endl;
        switch(tempProgram.charCodeAt(bufferCnt)){
            case 99:
                indiv.buffer += "HEAD ";
                indiv.buffer += "(";
                a1=this.recPrintIndividual(tempProgram, indiv, ++bufferCnt);

                break;
            case 100:
                indiv.buffer += "IFELSE ";
                indiv.buffer += "(";
                a1=this.recPrintIndividual(tempProgram, indiv, ++bufferCnt);

                break;
            case 101:
                indiv.buffer += "AND";
                indiv.buffer += "(";
                a1=this.recPrintIndividual(tempProgram, indiv, ++bufferCnt);

                break;
            case 102:
                indiv.buffer += "OR";
                indiv.buffer += "(";
                a1=this.recPrintIndividual(tempProgram, indiv, ++bufferCnt);

                break;
            case 103:
                indiv.buffer += "XOR";
                indiv.buffer += "(";
                a1=this.recPrintIndividual(tempProgram, indiv, ++bufferCnt);

                break;
            case 104:
                indiv.buffer += "LESS";
                indiv.buffer += "(";
                a1=this.recPrintIndividual(tempProgram, indiv, ++bufferCnt);

                break;
            case 105:
                indiv.buffer += "GREAT";
                indiv.buffer += "(";
                a1=this.recPrintIndividual(tempProgram, indiv, ++bufferCnt);

                break;
            case 106:
                indiv.buffer += "EQU";
                indiv.buffer += "(";
                a1=this.recPrintIndividual(tempProgram, indiv, ++bufferCnt);

                break;
            case 107:
                indiv.buffer += "ADD";
                indiv.buffer += "(";
                a1=this.recPrintIndividual(tempProgram, indiv, ++bufferCnt);

                break;
            case 108:
                indiv.buffer += "SUB";
                indiv.buffer += "(";
                a1=this.recPrintIndividual(tempProgram, indiv, ++bufferCnt);

                break;
            case 109:
                indiv.buffer += "MUL";
                indiv.buffer += "(";
                a1=this.recPrintIndividual(tempProgram, indiv, ++bufferCnt);

                break;
            case 110:
                indiv.buffer += "DIV";
                indiv.buffer += "(";
                a1=this.recPrintIndividual(tempProgram, indiv, ++bufferCnt);

                break;
            case 111:
               indiv.buffer += "MIN";
               indiv.buffer += "(";
               a1=this.recPrintIndividual(tempProgram, indiv, ++bufferCnt);

               break;
            case 112:
               indiv.buffer += "MAX";
               indiv.buffer += "(";
               a1=this.recPrintIndividual(tempProgram, indiv, ++bufferCnt);

               break;
            case 113:
               indiv.buffer += "NEG";
               indiv.buffer += "(";
               a1=this.recPrintIndividual(tempProgram, indiv, ++bufferCnt);

               break;
            case 114:
               indiv.buffer += "SIN";
               indiv.buffer += "(";
               a1=this.recPrintIndividual(tempProgram, indiv, ++bufferCnt);
               break;
            case 115:
               indiv.buffer += "COS";
               indiv.buffer += "(";
               a1=this.recPrintIndividual(tempProgram, indiv, ++bufferCnt);
               break;
            case 116:
               indiv.buffer += "TAN";
               indiv.buffer += "(";

               a1=this.recPrintIndividual(tempProgram, indiv, ++bufferCnt);
               break;
            case 117:
               indiv.buffer += "LOG";
               indiv.buffer += "(";

               a1=this.recPrintIndividual(tempProgram, indiv, ++bufferCnt);
               break;
            case 118:
               indiv.buffer += "MA";
               indiv.buffer += "(";

               a1=this.recPrintIndividual(tempProgram, indiv, ++bufferCnt);
               break;
            case 119:
               indiv.buffer += "MACD";
               indiv.buffer += "(";

               a1=this.recPrintIndividual(tempProgram, indiv, ++bufferCnt);
               break;
            case 120:
               indiv.buffer += "STOCH";
               indiv.buffer += "(";

               a1=this.recPrintIndividual(tempProgram, indiv, ++bufferCnt);
               break;
            case 121:
               indiv.buffer += "RSI";
               indiv.buffer += "(";

               a1=this.recPrintIndividual(tempProgram, indiv, ++bufferCnt);
               break;
            case 122:
                indiv.buffer += "MOM";
                indiv.buffer += "(";

                a1=this.recPrintIndividual(tempProgram, indiv, ++bufferCnt);
                break;
            case 123:
                indiv.buffer += "OPEN";
                indiv.buffer += "(";

                a1=this.recPrintIndividual(tempProgram, indiv, ++bufferCnt);
                break;
            case 124:
                indiv.buffer += "HIGH";
                indiv.buffer += "(";

                a1=this.recPrintIndividual(tempProgram, indiv, ++bufferCnt);
                break;
            case 125:
                indiv.buffer += "LOW";
                indiv.buffer += "(";

                a1=this.recPrintIndividual(tempProgram, indiv, ++bufferCnt);
                break;
            case 126:
                indiv.buffer += "CLOSE";
                indiv.buffer += "(";

                a1=this.recPrintIndividual(tempProgram, indiv, ++bufferCnt);
                break;
            case 127:
                indiv.buffer += "VOLUME";
                indiv.buffer += "(";

                a1=this.recPrintIndividual(tempProgram, indiv, ++bufferCnt);
                break;
            case 90:
                indiv.buffer += "LONG";
                indiv.buffer += "(";

                a1=this.recPrintIndividual(tempProgram, indiv, ++bufferCnt);
                break;
            case 91:
                indiv.buffer += "SHORT";
                indiv.buffer += "(";

                a1=this.recPrintIndividual(tempProgram, indiv, ++bufferCnt);
                break;
            case 92:
                indiv.buffer += "SL";
                indiv.buffer += "(";

                a1=this.recPrintIndividual(tempProgram, indiv, ++bufferCnt);
                break;
            case 93:
                indiv.buffer += "TP";
                indiv.buffer += "(";

                a1=this.recPrintIndividual(tempProgram, indiv, ++bufferCnt);
                break;
        }

        if(tempProgram.charCodeAt(bufferCnt - 1) > 100 && tempProgram.charCodeAt(bufferCnt - 1) < 113) {
          a2=this.recPrintIndividual(tempProgram, indiv, a1);
          indiv.buffer += ")";
          //console.log("var a2 100 - 113: ", a2, " : ", tempProgram.charCodeAt(bufferCnt - 1));
          return(a2);
        } else if (tempProgram.charCodeAt(bufferCnt - 1) > 129 && tempProgram.charCodeAt(bufferCnt - 1) < 132) {
            a2=this.recPrintIndividual(tempProgram, indiv, a1);
            indiv.buffer += ")";
            //console.log("var a2 129 - 132: ", a2, " : ", tempProgram.charCodeAt(bufferCnt - 1));
            return(a2);
        } else if (tempProgram.charCodeAt(bufferCnt - 1) > 98 && tempProgram.charCodeAt(bufferCnt - 1) < 101) {
            a2=this.recPrintIndividual(tempProgram, indiv, a1);
            a3=this.recPrintIndividual(tempProgram, indiv, a2);
            indiv.buffer += ")";
            //console.log("var a3 98 - 101: ", a3, " : ", tempProgram.charCodeAt(bufferCnt - 1));
            return(a3);
        }   else if (tempProgram.charCodeAt(bufferCnt - 1) > 89 && tempProgram.charCodeAt(bufferCnt - 1) < 92) {
            a2=this.recPrintIndividual(tempProgram, indiv, a1);
            indiv.buffer += ")";
            //console.log("var a2 89 - 92: ", a2, " : ", tempProgram.charCodeAt(bufferCnt - 1));
            return(a2);
        } else if (tempProgram.charCodeAt(bufferCnt - 1) > 117 && tempProgram.charCodeAt(bufferCnt - 1) < 123) {
            a2=this.recPrintIndividual(tempProgram, indiv, a1);
            a3=this.recPrintIndividual(tempProgram, indiv, a2);
            a4=this.recPrintIndividual(tempProgram, indiv, a3);
            a5=this.recPrintIndividual(tempProgram, indiv, a4);
            indiv.buffer += ")";
            //console.log("var a5 117 - 123: ", a5, " : ", tempProgram.charCodeAt(bufferCnt - 1));
            return(a5);
        }  else if (tempProgram.charCodeAt(bufferCnt - 1) < 90) {
           indiv.buffer += ",";
           //console.log("var a1 <90: ", a1, " : ", tempProgram.charCodeAt(bufferCnt - 1));
          return(a1);
        } else {
           indiv.buffer += ")";
           //console.log("var a1 done: ", a1, " : ", tempProgram.charCodeAt(bufferCnt - 1));
          return(a1);
        }
    };

}


function Ledger() {
    this.initialBalance = 10000;
    this.balance = this.initialBalance;
    this.curentSize = 1.0;
    this.tradeRegister = [];

    this.getTotalReturn = function() {
        var result = 0;
        for (x in this.tradeRegister) {
            result += x.getPositionReturn();
        }
        return result;
    }

    this.getMaxDrawdown = function(){
        var peak = this.initialBalance;
        var trough = this.initialBalance;
        this.balance = this.initialBalance;
        var drawdown = 0;
        var reset = false;

        for (x in this.tradeRegister) {
            this.balance += x.getPositionReturn();
            if (this.balance >= peak) {
                peak = this.balance;
                reset = true;
                trough = this.balance;
            } else if (trough >= this.balance) {
                trough = this.balance;
                if (drawdown <= peak - trough) {
                   drawdown = peak - trough;
                }
            } else {
                reset = false;
            }
        }
        return drawdown;
    }

    this.getTradeDistribution = function() {
        var longs = 0;
        var shorts = 0;
        for (x  in this.tradeRegister) {
            if (x.getDirection() == "Long") {
                //std::cout << "Long" << std::endl;
                ++longs;
            } else {
                ++shorts;
                //std::cout << "Short" << std::endl;
            }
        }
        if (longs == 0 || shorts == 0) {
            return 0;
        } else {
            return Math.min(longs, shorts) / Math.max(longs, shorts);
        }
    }

    this.addTrade = function(newTrade) {
        this.tradeRegister.push(newTrade);
        this.balance += newTrade.getPositionReturn();
    }

    this.clearTrades = function () {
        if (this.tradeRegister.size() > 0) {
            this.tradeRegister.clear();
            this.currentSize = 1.0;
            this.balance = this.initialBalance;
        }
    }

    this.getTrade = function(index) {
        return this.tradeRegister[index];
    }

    this.getTradeByDate = function(openDate) {
        var pos = 0;
        for (x in this.tradeRegister) {
            if (x.getOpenDate() == openDate) {
                return this.tradeRegister[pos];
            }
            pos++;
        }
    }

    this.getBalance = function () {
        return this.balance;
    }

    this.getInitialBalance = function() {
        return this.initialBalance;
    }
}

/*
void Ledger::outputTrades() {
    std::cout << "---------------------------------------------------------Ledger--------------------------------------------------------------" << std::endl;
    std::cout << "Number\tDirection\tSize\tOpen Date\tClose Date\tOpen Price\tClose Price\tTP\t\tSL\t\tPosition Return" << std::endl;
    int cnt = 1;
    float TP = 0;
    float SL = 0;
    for (Trade it : tradeRegister) {
        if (it.getDirection() == "Long") {
            TP = it.getLongTP();
            SL = it.getLongSL();
        } else {
            TP = it.getShortTP();
            SL = it.getShortSL();
        }
        std::cout << cnt << "\t" << it.getDirection() << "\t\t" << it.getPositionSize() << "\t" << it.getOpenDate() << "\t" << it.getCloseDate() << "\t" << it.getOpenValue() << "\t\t" << it.getCloseValue()
            << "\t\t" << TP << "\t\t" << SL << "\t\t" << it.getPositionReturn() << std::endl;
        cnt++;
    }
    std::cout << "Total Return: " << this->getTotalReturn() << std::endl;
}
*/

