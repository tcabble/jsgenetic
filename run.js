//self.importScripts('individual.js');

function Run(chartdata, runNo) {

    this.chartdata = chartdata;
    this.params = new Params();
    this.population = [];
    this.runNo = runNo;
    this.fitnessFunction = new FitnessFunction();
    this.evaluator = new Evaluator(this.chartdata, range[0]);

    this.nextInt = function(range) {
        return Math.floor(Math.random() * range);
    };

    this.createRandomPopulation = function(){
        for (var i = 0; i < this.params.populationSize; i++) {
            var dist = 0;
            while (dist < 0.05) {
                var newIndiv = new Individual();
                this.createRandomIndividual(newIndiv);
                this.fitnessFunction.calcFitness(this.evaluator, newIndiv, range);
                this.fitnessFunction.calcValFitness(this.evaluator, newIndiv, range);
                this.params.progsEvaled++;
                dist = newIndiv.getLedgers(0).getTradeDistribution();
                if (dist > 0.05) {
                    this.population.push(newIndiv);
                }
            }
        }
            //this->test->getEngine()->demeIsReady(this->getRunNumber());
    };

    this.createRandomIndividual = function(individual){
        var len = 0;
        len = this.grow(individual, 0, this.params.maxDepth, "root");
        while (len > this.params.maxLength)
        {
           individual.program = "";
           len = this.grow(individual, 0, this.params.maxDepth, "root");
        }
        this.growTrade(individual, individual.program.length, this.params.maxDepth, "long");
        this.growTrade(individual, individual.program.length, this.params.maxDepth, "short");
        for (var i = 0; i < 3; ++i) {
            var ledger = new Ledger();
            individual.setLedger(ledger, i);
        }
    };


    this.grow = function(individual, pos, depth, type){
        var random = 0;
        var node = this.nextInt(100);



        if (depth < 0) {
            return 1000;
        }

        if (type == "root") {
            node = 99;
            //std::cout << (int)node << std::endl;
            individual.program += String.fromCharCode(node);
            return  this.grow(individual, pos+1, depth-1, "bool");
        } else if (type == "bool") {
            node = this.nextInt(this.params.EQU - this.params.AND + 1) + this.params.AND;// (char) test->nextInt(params.EQU - params.AND + 1) + params.AND;
            switch (node){

                case 101:
                case 102:
                case 103:
                    individual.program += String.fromCharCode(node);
                    return(this.grow(individual, this.grow(individual, pos+1, depth-1, "bool"), depth-1, "bool"));
                case 104:
                case 105:
                case 106:
                    individual.program += String.fromCharCode(node);
                    return(this.grow(individual, this.grow(individual, pos+1, depth-1, "any"), depth-1, "any"));
            }

        } else if (node < 50 || depth <= 0) {
            node = this.nextInt(this.params.VOLUME - this.params.MA + 1) + this.params.MA;// test->nextInt(params.VOLUME - params.MA + 1) + params.MA;
            switch (node) {
                case 118:
                case 119:
                case 120:
                case 121:
                case 122:
                    individual.program += String.fromCharCode(node);
                    individual.program += String.fromCharCode(this.nextInt(32 - 3) + 3);//push_back((char)(this.nextInt(32 - 3) + 3));
                    individual.program += String.fromCharCode(this.nextInt(15 - 3) + 3);//push_back((char)(test->nextInt(15 - 3) + 3));
                    individual.program += String.fromCharCode(this.nextInt(15 - 3) + 3);//push_back((char)(test->nextInt(15 - 3) + 3));
                    individual.program += String.fromCharCode(this.nextInt(40));//push_back((char)test->nextInt(40));
                    individual.program += String.fromCharCode(this.nextInt(4));//push_back((char)test->nextInt(4));
                    return (pos + 6);
                case 123:
                case 124:
                case 125:
                case 126:
                case 127:
                    individual.program += String.fromCharCode(node);
                    individual.program += String.fromCharCode(this.nextInt(40));//push_back((char)test->nextInt(40));
                    return (pos + 2);
            }

        } else {
            node = 0;
            while (true){
                node = this.nextInt(this.params.LOG - this.params.IFELSE + 1) + this.params.IFELSE;// test->nextInt(params.LOG - params.IFELSE + 1) + params.IFELSE;
                if (node == 100 || (node > 106 && node < 118)) break;
            }

            //std::cout << "Func" << (int)node << std::endl;
            switch(node) {
                case 100:
                    individual.program += String.fromCharCode(node);
                    return(this.grow(individual, this.grow(individual,  this.grow(individual, pos+1, depth-1, "bool"), depth-1, "any"), depth-1, "any"));
                case 107:
                case 108:
                case 109:
                case 110:
                case 111:
                case 112:
                    individual.program += String.fromCharCode(node);
                    return(this.grow(individual, this.grow(individual, pos+1, depth-1, "any"), depth-1, "any"));

                case 113:
                case 114:
                case 115:
                case 116:
                case 117:
                    individual.program += String.fromCharCode(node);
                    return(this.grow(individual, pos+1, depth-1, "any"));
            }
        }

        return (0);
    };

    this.growTrade = function(individual, pos, depth, type){

        var node = this.nextInt(100);

        //console.log(type, " : ", node);

        if (type == "long") {
            node = 90;
            individual.program += String.fromCharCode(node);
            return(this.growTrade(individual, this.growTrade(individual, pos + 1, depth - 1, "SL"), depth - 1, "TP"));
        } else if (type == "short") {
            node = 91;
            individual.program += String.fromCharCode(node);
            return(this.growTrade(individual, this.growTrade(individual, pos + 1, depth - 1, "SL"), depth - 1, "TP"));
        } else if (type == "SL") {
            node = 92;
            individual.program += String.fromCharCode(node);
            return(this.growTrade(individual, pos + 1, depth - 1, "any"));
        } else if (type == "TP") {
            node = 93;
            individual.program += String.fromCharCode(node);
            return(this.growTrade(individual, pos + 1, depth - 1, "any"));
        } else if (node < 50 || depth <= 0) {
            node = this.nextInt(this.params.VOLUME - this.params.MA + 1) + this.params.MA;
            //console.log("Indicators : ", node);
            switch (node) {
                case 118:
                case 119:
                case 120:
                case 121:
                case 122:
                    individual.program += String.fromCharCode(node);
                    individual.program += String.fromCharCode(this.nextInt(32 - 3) + 3);//push_back((char)(this.nextInt(32 - 3) + 3));
                    individual.program += String.fromCharCode(this.nextInt(15 - 3) + 3);//push_back((char)(test->nextInt(15 - 3) + 3));
                    individual.program += String.fromCharCode(this.nextInt(15 - 3) + 3);//push_back((char)(test->nextInt(15 - 3) + 3));
                    individual.program += String.fromCharCode(this.nextInt(40));//push_back((char)test->nextInt(40));
                    individual.program += String.fromCharCode(this.nextInt(4));//push_back((char)test->nextInt(4));
                    return (pos + 6);
                case 123:
                case 124:
                case 125:
                case 126:
                case 127:
                    individual.program += String.fromCharCode(node);
                    individual.program += String.fromCharCode(this.nextInt(40));//push_back((char)test->nextInt(40));
                    return (pos + 2);
            }

        } else {
            node = this.nextInt(this.params.FSET_END - this.params.FSET_START + 1) + this.params.FSET_START;
            //console.log("Functions : ", node);
            switch(node) {
                case 107:
                case 108:
                case 109:
                case 110:
                case 111:
                case 112:
                    individual.program += String.fromCharCode(node);
                    return(this.growTrade(individual, this.growTrade(individual, pos+1, depth-1, "any"), depth-1, "any"));

                case 113:
                case 114:
                case 115:
                case 116:
                case 117:
                    individual.program += String.fromCharCode(node);
                    return(this.growTrade(individual, pos+1, depth-1, "any"));
            }
        }
        return (0);
    };

}


