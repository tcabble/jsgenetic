function Params() {

    this.populationSize = 100;
    this.maxLength = 120;
    this.maxDepth = 5;

    this.HEAD = 99;

	this.IFELSE = 100;
	this.AND = 101;
	this.OR = 102;
	this.XOR = 103;
	this.LESS = 104;
	this.GREAT = 105;
	this.EQU = 106;
	this.ADD = 107;
	this.SUB = 108;
	this.MUL = 109;
	this.DIV = 110;
	this.MIN = 111;
	this.MAX = 112;
	this.NEG = 113;
	this.SIN = 114;
	this.COS = 115;
	this.TAN = 116;
	this.LOG = 117;
    this.MA = 118;
	this.MACD = 119;
	this.STOCH = 120;
	this.RSI = 121;
	this.MOM = 122;
	this.OPEN = 123;
	this.HIGH = 124;
	this.LOW= 125;
	this.CLOSE = 126;
	this.VOLUME = 127;
	this.FSET_START = this.ADD;
	this.FSET_END = this.LOG;

	this.TRADE_LONG = 90;
	this.TRADE_SHORT = 91;
	this.SL = 92;
	this.TP = 93;
	this.TRADESET_START = 90;
	this.TRADESET_END = 93;
}
