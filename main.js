var worker, worker2;

var range;

function initWorkers() {
    worker = new Worker("worker.js");

    worker.addEventListener('message', function(e){
        //console.log(e.data);
        if (e.data.cmd == 'print') {
            //var pop = JSON.parse(e.data.population);
            var pop = e.data.population;

            jQuery.each(pop, function(){
                console.log('Worker Individual: ', this.toString());

                //var integers = [];
                //var i = 0;
                //for (i = 0; i < this.program.length; ++i) {
                //    integers.push(this.program.charCodeAt(i));
                //}
                //jQuery.each(this.program, function(index){
                //    integers.push(this.program.charCodeAt(index));
                //});
                //console.log(integers);
                //console.log(this);
            });
        } else if (e.data.cmd == 'chart') {
            var ohlc = [];
            jQuery.each(e.data.data, function(){

                ohlc.push([this.STARTTIME, parseFloat(this.OPEN), parseFloat(this.HIGH), parseFloat(this.LOW), parseFloat(this.CLOSE)]);
            });

            var mainGraphOptions = {
                  title: 'Full Series',
                  axesDefaults:{
                        rendererOptions: {
                        alignTicks: true
                      }
                  },
                  seriesDefaults:{yaxis:'y2axis'},
                  axes: {
                      xaxis: {
                          renderer:$.jqplot.DateAxisRenderer
                      },
                      y2axis: {
                          tickOptions:{formatString:'$%.3f'}
                      }
                  },
                  series: [{renderer:$.jqplot.OHLCRenderer, rendererOptions:{candleStick:true, upBodyColor:'green', downBodyColor:'red', wickColor:'black'}}],
                  cursor:{
                        show: true,
                      zoom:true,
                      tooltipOffset: 10,
                      tooltipLocation: 'nw'
                  },
                  highlighter: {
                      show: true,
                      showMarker:false,
                      tooltipAxes: 'xy',
                      yvalues: 4,
                      formatString:'<table class="jqplot-highlighter"> \
                      <tr><td>date:</td><td>%s</td></tr> \
                      <tr><td>open:</td><td>%s</td></tr> \
                      <tr><td>hi:</td><td>%s</td></tr> \
                      <tr><td>low:</td><td>%s</td></tr> \
                      <tr><td>close:</td><td>%s</td></tr></table>'
                  },
                  grid: {
                        background : 'dimgray'
                  }
                }

            var zoomGraphOptions = {
                  title: 'Selected Range',
                  axesDefaults:{
                        rendererOptions: {
                        alignTicks: true
                      }
                  },
                  seriesDefaults:{yaxis:'y2axis'},
                  axes: {
                      xaxis: {
                          renderer:$.jqplot.DateAxisRenderer
                      },
                      y2axis: {
                          tickOptions:{formatString:'$%.3f'}
                      }
                  },
                  series: [{renderer:$.jqplot.OHLCRenderer, rendererOptions:{candleStick:true, upBodyColor:'green', downBodyColor:'red', wickColor:'black'}}],
                  cursor:{
                        show: true,
                      zoom:true,
                      tooltipOffset: 10,
                      tooltipLocation: 'nw'
                  },
                  highlighter: {
                      show: true,
                      showMarker:false,
                      tooltipAxes: 'xy',
                      yvalues: 4,
                      formatString:'<table class="jqplot-highlighter"> \
                      <tr><td>date:</td><td>%s</td></tr> \
                      <tr><td>open:</td><td>%s</td></tr> \
                      <tr><td>hi:</td><td>%s</td></tr> \
                      <tr><td>low:</td><td>%s</td></tr> \
                      <tr><td>close:</td><td>%s</td></tr></table>'
                  },
                  grid: {
                        background : 'dimgray'
                  }
                }

            if (jQuery('#zoomchartdiv').is(':visible')) {
                jQuery('#zoomchartdiv').empty();
                jQuery('#mainchartdiv').empty();
                plot2.destroy();
                plot1.destroy();
                plot1 = $.jqplot('zoomchartdiv',[ohlc], zoomGraphOptions);
                plot2 = $.jqplot('mainchartdiv',[ohlc], mainGraphOptions);
                $.jqplot.Cursor.zoomProxy(plot1, plot2);
            } else {
                jQuery('#zoomchartdiv').show();
                jQuery('#mainchartdiv').show();
                plot1 = $.jqplot('zoomchartdiv',[ohlc], zoomGraphOptions);
                plot2 = $.jqplot('mainchartdiv',[ohlc], mainGraphOptions);
                $.jqplot.Cursor.zoomProxy(plot1, plot2);
            }
        } else if (e.data.cmd == 'error') {
            alert(e.data.message);
            return false;
        }

    }, false);

    worker2 = new Worker("worker.js");

    worker2.addEventListener('message', function(e){
        console.log(e.data);
        if (e.data.cmd == 'print') {
            //var pop = JSON.parse(e.data.population);
            var pop = e.data.population;

            jQuery.each(pop, function(){
                console.log('Worker Individual: ', this.toString());

                //var integers = [];
                //var i = 0;
                //for (i = 0; i < this.program.length; ++i) {
                //    integers.push(this.program.charCodeAt(i));
                //}
                //jQuery.each(this.program, function(index){
                //    integers.push(this.program.charCodeAt(index));
                //});
                //console.log(integers);
                //console.log(this);
            });
        } else if (e.data.cmd == 'chart') {

        }

    }, false);

}

function workerTest() {

    var params = new Params();

    worker.postMessage({'cmd' : 'run', 'msg' : params});
    //worker2.postMessage({'cmd' : 'run', 'msg' : params});

}

function loadTest() {
    var fileInput = document.getElementById('filePicker');

    var file = fileInput.files[0];

    worker.postMessage({'cmd' : 'loadData', 'fileObj' : file});
}

function getRange() {
    console.log(range);
}

function testTALib() {
    /*
    define (['require', '/talib/build/Release/talib'], function (dynModules) {
        require(dynModules, function(){
            console.log(dynModules);
            // use arguments since you don't know how many modules you're getting in the callback
            for (var i = 0; i < arguments.length; i++){
                var mymodule = arguments[i];
                console.log("TALib Version: " + mymodule.version);
            }
        });
        //var thistalib = require('/talib/build/Release/talib');
        //console.log("TALib Version: " + thistalib.version);
        //return thistalib
    });



    var talib = require(['require', './talib/build/Release/talib'], function (require) {
        var namedModule = require('./talib/build/Release/talib');
        console.log("TALib Version: " + namedModule.version);
    });
    */

    var talib = require('/build/Release/talib');
    console.log("TALib Version: " + talib.version);

    // Display all available indicator function names
    var functions = talib.functions;
    for (i in functions) {
        console.log(functions[i].name);
    }
}













