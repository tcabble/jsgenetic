function Ledger() {
    this.initialBalance = 10000;
    this.balance = this.initialBalance;
    this.curentSize = 1.0;
    this.tradeRegister = [];

    this.getTotalReturn = function() {
        var result = 0;
        for (x in this.tradeRegister) {
            result += x.getPositionReturn();
        }
        return result;
    }

    this.getMaxDrawdown = function(){
        var peak = this.initialBalance;
        var trough = this.initialBalance;
        this.balance = this.initialBalance;
        var drawdown = 0;
        var reset = false;

        for (x in this.tradeRegister) {
            this.balance += x.getPositionReturn();
            if (this.balance >= peak) {
                peak = this.balance;
                reset = true;
                trough = this.balance;
            } else if (trough >= this.balance) {
                trough = this.balance;
                if (drawdown <= peak - trough) {
                   drawdown = peak - trough;
                }
            } else {
                reset = false;
            }
        }
        return drawdown;
    }

    this.getTradeDistribution = function() {
        var longs = 0;
        var shorts = 0;
        for (x  in this.tradeRegister) {
            if (x.getDirection() == "Long") {
                //std::cout << "Long" << std::endl;
                ++longs;
            } else {
                ++shorts;
                //std::cout << "Short" << std::endl;
            }
        }
        if (longs == 0 || shorts == 0) {
            return 0;
        } else {
            return Math.min(longs, shorts) / Math.max(longs, shorts);
        }
    }

    this.addTrade = function(newTrade) {
        this.tradeRegister.push(newTrade);
        this.balance += newTrade.getPositionReturn();
    }

    this.clearTrades = function () {
        if (this.tradeRegister.size() > 0) {
            this.tradeRegister.clear();
            this.currentSize = 1.0;
            this.balance = this.initialBalance;
        }
    }

    this.getTrade = function(index) {
        return this.tradeRegister[index];
    }

    this.getTradeByDate = function(openDate) {
        var pos = 0;
        for (x in this.tradeRegister) {
            if (x.getOpenDate() == openDate) {
                return this.tradeRegister[pos];
            }
            pos++;
        }
    }

    this.getBalance = function () {
        return this.balance;
    }

    this.getInitialBalance = function() {
        return this.initialBalance;
    }
}

/*
void Ledger::outputTrades() {
    std::cout << "---------------------------------------------------------Ledger--------------------------------------------------------------" << std::endl;
    std::cout << "Number\tDirection\tSize\tOpen Date\tClose Date\tOpen Price\tClose Price\tTP\t\tSL\t\tPosition Return" << std::endl;
    int cnt = 1;
    float TP = 0;
    float SL = 0;
    for (Trade it : tradeRegister) {
        if (it.getDirection() == "Long") {
            TP = it.getLongTP();
            SL = it.getLongSL();
        } else {
            TP = it.getShortTP();
            SL = it.getShortSL();
        }
        std::cout << cnt << "\t" << it.getDirection() << "\t\t" << it.getPositionSize() << "\t" << it.getOpenDate() << "\t" << it.getCloseDate() << "\t" << it.getOpenValue() << "\t\t" << it.getCloseValue()
            << "\t\t" << TP << "\t\t" << SL << "\t\t" << it.getPositionReturn() << std::endl;
        cnt++;
    }
    std::cout << "Total Return: " << this->getTotalReturn() << std::endl;
}
*/
